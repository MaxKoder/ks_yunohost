<?php
$jsonFile = getenv("tmpDir") . "ynhvars.json";

if ($argc === 1) {
    /* No args, return number of disks */
    echo count(json_decode(file_get_contents($jsonFile),true)['system']['disks']);
} else {
    $jsonDisks = json_decode(file_get_contents($jsonFile),true)['system']['disks'];
    $keys = array_keys($jsonDisks);
    echo $keys[$argv[1]] . ' : ' . $jsonDisks[$keys[$argv[1]]];
}

?>
