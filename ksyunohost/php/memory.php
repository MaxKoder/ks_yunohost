<?php

$jsonFile = getenv("tmpDir") . "ynhvars.json";
$json = json_decode(file_get_contents($jsonFile),true)['system']['memory'];

if ($argc === 1) {
    /* No args, return all */
    foreach ($json as $key => $value) {
        echo $key . ' : ' . $value . "\n";
    }
} else {
    if (array_key_exists($argv[1], $json)) {
        echo $argv[1] . ' : ' . $json[$argv[1]];
    } else {
        echo 'Memory ' . $argv[1] . ' not found';
    }
}

?>
