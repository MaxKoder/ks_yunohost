<?php

$jsonFile = getenv("tmpDir") . "ynhvars.json";
$json = json_decode(file_get_contents($jsonFile),true)['packages'];

$sys = array('yunohost', 'yunohost-admin', 'moulinette', 'ssowat');

if ($argc === 1) {
    /* No args, return all */
    foreach ($sys as $s) {
        echo $s . ' : ' . $json[$s] . "\n";
    }
} else {
    if (in_array($argv[1], $sys)) {
        echo $argv[1] . ' : ' . $json[$argv[1]];
    } else {
        echo 'System app ' . $argv[1] . ' not installed';
    }
}

?>
