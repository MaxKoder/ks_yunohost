#!/bin/bash

let "numberDisks=$(php -f $phpDir'disks.php')"

declare -a ynhDisks

for i in `seq 1 $numberDisks`;
do
    let "currDisk =$i-1"
    ynhDisks[$currDisk]=$(php -f $phpDir'disks.php' $currDisk)
    echo ${ynhDisks[$currDisk]}
done
