#!/bin/bash

echo 
echo "- Retrieving datas from Yunohost server"
if [ "$ksyTest" = "1" ];then
    echo $(cat $scriptDir"ksyunohost/tests/testyunohostvars.json") > $tmpDir"ynhvars.json"
else
    echo $(sudo yunohost tools diagnosis --output-as json) > $tmpDir"ynhvars.json"
fi
