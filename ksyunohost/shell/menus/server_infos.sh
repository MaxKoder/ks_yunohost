#!/bin/bash

# Server Infos Menu

option=$(whiptail --title "Server Infos" --menu "Choose what you want" \
    --cancel-button "Back to Main Menu" 15 60 8 \
    "1" "Disks Infos" \
    "2" "System Versions" \
    "3" "Memory" \
    "4" "System Infos (glances)" \
    "5" "View Process (top)"  3>&1 1>&2 2>&3)

exitstatus=$?

case "$option" in
1) whiptail --title "Disks Infos" --msgbox "$($shUtilsDir'get_disks_list.sh')" 10 60
;;
2) whiptail --title "System Versions" --msgbox "$($shUtilsDir'system_versions.sh')" 10 60
;;
3) whiptail --title "Memory" --msgbox "$($shUtilsDir'memory.sh')" 10 60
;;
4) sudo glances
;;
5) sudo top
;;
esac
if [ $exitstatus -ne 0 ]; then
    $shMenusDir"main.sh"
    exit
fi

$shMenusDir"server_infos.sh"
