#!/bin/bash

# Main menu

option=$(whiptail --title "KS Yunohost v$ksyVersion" --menu "Choose what you want" \
    --cancel-button "Quit" 15 60 8 \
    "1" "Server Infos" \
    "2" "Backups" \
    "3" "Apps" \
    "4" "Users"  \
    "5" "About KS Yunohost" 3>&1 1>&2 2>&3)

exitstatus=$?

case "$option" in
1) $shMenusDir"server_infos.sh"
;;
3) $shMenusDir"apps.sh"
;;
0) break
;;
esac
if [ $exitstatus -ne 0 ]; then
    exit   
fi

