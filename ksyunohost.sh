#!/bin/bash

##########################################################################
#   _   __ _____  __   __                      _                  _      #
#  | | / //  ___| \ \ / /                     | |                | |     #
#  | |/ / \ `--.   \ V /  _   _  _ __    ___  | |__    ___   ___ | |_    #
#  |    \  `--. \   \ /  | | | || '_ \  / _ \ | '_ \  / _ \ / __|| __|   #
#  | |\  \/\__/ /   | |  | |_| || | | || (_) || | | || (_) |\__ \| |_    #
#  \_| \_/\____/    \_/   \__,_||_| |_| \___/ |_| |_| \___/ |___/ \__|   #
#                                                                        #
#                                                                        #
#                         Keep Simple Yunohost                           #
#                                                                        #
# Simple tool to manage Yunohost server with Bash                        #
#                                                                        #
# Made with love by Max Koder : https://max-koder.fr                     #
#                                                                        #
##########################################################################


# Export Globals Vars to use them in others scripts

export ksyTest=1

export ksyVersion="0.1"

export scriptDir=$(dirname $0)"/"
export shDir=$scriptDir"ksyunohost/shell/"
export shMenusDir=$shDir"menus/"
export shUtilsDir=$shDir"utils/"
export phpDir=$scriptDir"ksyunohost/php/"
export tmpDir=$scriptDir"ksyunohost/tmp/"

clear

# Init Yunohost Vars
$shDir"init_yunohost_vars.sh"

# Launch Main Menu
$shMenusDir"main.sh"

sleep 5
